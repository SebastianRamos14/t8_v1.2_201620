package estructuras;




/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> {

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private NodoGrafo[] nodos; 

	/**
	 * Lista de adyacencia 
	 */
	private TablaHashChaining<K, Arco> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
		// Es libre de implementarlo con la representacion de su agrado. 

	
	private int N;
	
	private int M;
	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido(int tam) {
		//TODO implementar}
		nodos = new NodoGrafo[tam];
		N = 0;
		M = 0;
	}
	public GrafoNoDirigido() {
		//TODO implementar}
		nodos = new NodoGrafo[0];
		N = 0;
		M = 0;
	}

	@Override
	public boolean agregarNodo(Nodo<K> nodo) {
		//TODO implementar
		boolean resultado = true;
		for (int i = 0; i < nodos.length && resultado; i++) {
			if(nodos[i] != null)
			{
				if(nodos[i].darId().compareTo(nodo.darId()) == 0)
				resultado = false;
			}
			else
			resultado = false;
		}
		if(resultado)
		{
		nodos[M] = (NodoGrafo) nodo;
		M++;
		}
		
		return resultado;
	}

	@Override
	public boolean eliminarNodo(K id) {
		//TODO implementar
		boolean resultado = false;
		for (int i = 0; i < nodos.length && !resultado; i++) {
			if(nodos[i].darId().compareTo(id) == 0)
			{
			nodos[i] = null;
			resultado = true;
			}
		}
		return resultado;
	}

	@Override
	public Arco<K,E>[] darArcos() {
		//TODO implementar
		 Arco<K,E>[] arcos = new Arco[N];
		 int tam = 0;
		 ListaHash<K, Arco>[] x = adj.darListas();
		 for (int i = 0; i < x.length; i++) {
			ListaHash<K, Arco> y = x[i];
			NodoHashChaining<K, Arco> temp = y.primero;
			while(temp != null)
			{
				arcos[tam]=(Arco<K, E>) temp.getValor();
				tam++;
			}
		}
		 return arcos;
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		{
			return null;
		}
	}

	@Override
	public Nodo<K>[] darNodos() {
		//TODO implementar
		return nodos;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj) {
		//TODO implementar
		boolean resultado = false;
		Arco arcoAgregar = crearArco(inicio, fin, costo, obj);
		if(arcoAgregar != null)
		{
			resultado = true;
			adj.put(inicio, arcoAgregar);
			N++;
		}
		return resultado;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) {
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin) {
		//TODO implementar
		Arco<K,E> rta = adj.get(inicio);
		if(rta != null)
		{
			adj.put(inicio, null);
		}
		return null;
	}

	@Override
	public Nodo<K> buscarNodo(K id) {
		//TODO implementar
		Nodo<K> rta = null;
		boolean enc = false;
		for (int i = 0; i < nodos.length && !enc; i++) {
			if(nodos[i].darId().compareTo(id) == 0)
			{
				rta = nodos[i];
				enc = true;
			}
		}
		return rta;
	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) {
		//TODO implementar
		Arco<K,E>[] arcos = darArcos();
		Arco<K,E>[] rta = new Arco[N];
		int tam = 0;
		for (int i = 0; i < arcos.length; i++) {
			if(arcos[i].darNodoInicio().darId().compareTo(id) == 0)
			{
				rta[tam] = arcos[i];
				tam++;
			}
		}
		return rta;
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) {
		//TODO implementar
		Arco<K,E>[] arcos = darArcos();
		Arco<K,E>[] rta = new Arco[N];
		int tam = 0;
		for (int i = 0; i < arcos.length; i++) {
			if(arcos[i].darNodoFin().darId().compareTo(id) == 0)
			{
				rta[tam] = arcos[i];
				tam++;
			}
		}
		return rta;
	}

}
