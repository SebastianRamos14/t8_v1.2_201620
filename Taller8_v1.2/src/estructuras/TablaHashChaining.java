package estructuras;



public class TablaHashChaining<K ,V>
{

	/**
	 * Tama�o del arreglo fijo. 
	 */
	private int capacidad;
	
	public int llavesActuales1;

	private ListaHash<K, V>[] listas;

	public TablaHashChaining () {
		this(997);
	}
	public TablaHashChaining(int size)
	{
		this.capacidad = size;
		listas = new ListaHash[capacidad];
		for (int i = 0; i < capacidad; i++)
			listas[i] = new ListaHash();
	}
	private int hash(K pllave)
	{
		int llave = (Integer)pllave;
		return llave%listas.length;

	}

	public ListaHash<K, V>[] darListas()
	{
		return listas;
	}
	public V get(K llave)
	{
		return (V) listas[hash(llave)].get(llave);
	}
	public void put(K llave, V valor)
	{
		listas[hash(llave)].put(llave,valor);
		llavesActuales1++;
	}
}
