package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema de metro de New York
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";
	
	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";

	
	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	//TODO Declare el atributo grafo No dirigido que va a modelar el sistema. 
	// Los identificadores de las estaciones son String
	private GrafoNoDirigido<String, Estacion> grafo;
	
	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		//TODO inicialice el grafo como un GrafoNoDirigido
		grafo = new GrafoNoDirigido();
	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		//TODO Implementar
		Arco[] x = grafo.darArcosOrigen(identificador);
		String[] rta = new String[x.length];
		for (int i = 0; i < rta.length; i++) {
			rta[i] = (String) ((Estacion) x[i].darInformacion()).darId();
		}
		return rta;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public double distanciaMinimaEstaciones()
	{
		//TODO Implementar
		return 0.0;
	}
	
	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaEstaciones()
	{
		//TODO Implementar
		return 0.0;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar
		FileReader f = new FileReader(RUTA_PARADEROS);
		BufferedReader br = new BufferedReader(f);
		String cadena = br.readLine();
		int N =Integer.parseInt(cadena);
		grafo = new GrafoNoDirigido(N);

		Estacion x = null;
		while(N > 0)
		{
			cadena = br.readLine();
			String[] y = cadena.split(";");
			x = new Estacion(y[0], Double.parseDouble(y[1]), Double.parseDouble(y[2]));
			grafo.agregarNodo(x);
			N--;
		}
		br.close();
		System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" Estaciones");
		
		FileReader f1 = new FileReader(RUTA_RUTAS);
		BufferedReader br1 = new BufferedReader(f1);
		String cadena1 = br1.readLine();
		int M = Integer.parseInt(cadena1);
		while(M > 0)
		{
			br1.readLine();
			br1.readLine();
			cadena1 = br1.readLine();			
			int V = Integer.parseInt(cadena1);
			String est1 = br1.readLine();
			for (int i = 1; i < V; i++) {
				String[] bd = br1.readLine().split(" ");
				grafo.agregarArco(est1, bd[0], Double.parseDouble(bd[1]));
			}
			M--;
		}
		//TODO Implementar
		br1.close();
		System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" arcos");
	}

	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		//TODO Implementar
		Estacion<String> rta = (Estacion<String>) grafo.buscarNodo(identificador);	
		return rta;
	}

}
