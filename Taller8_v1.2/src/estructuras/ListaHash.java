package estructuras;

public class ListaHash <K, V>{

	NodoHashChaining primero;

	public V get(K llave)
	{
		for (NodoHashChaining x = primero; x != null; x = x.siguiente) {
			if(x.getLlave().equals(llave))
				return (V) x.getValor();
		}
		return null;
	}

	public void put(K llave, V valor)
	{
		for(NodoHashChaining x = primero; x != null; x = x.siguiente)
		{
			x.setValor(valor); return;
		}
		primero = new NodoHashChaining<K, V>(llave, valor, primero);
	}
}
